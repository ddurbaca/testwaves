//
//  MetalView.swift
//  test
//
//  Created by Dan Durbaca on 11/10/2020.
//  Copyright © 2020 Dan Durbaca. All rights reserved.
//

import Foundation
import MetalKit
import MetalPerformanceShaders
import CoreVideo
 
@objc public enum PixelResizeMode: Int {
    case scaleToFill = 1
    case scaleAspectFit = 2
    case scaleAspectFill = 3
}

final class MetalView: MTKView {
    
    var firstFilterEnabled:Bool = false
    var secondFilterEnabled:Bool = false
    var thirdFilterEnabled:Bool = false

    var inputTime: CFTimeInterval?
     
    var pixelBuffer: CVPixelBuffer? {
        didSet {
            setNeedsDisplay()
        }
    }
     
    var videoPixelBuffer: CVPixelBuffer? {
        didSet {
            setNeedsDisplay()
        }
    }
    
    private var textureCache: CVMetalTextureCache?
    private var commandQueue: MTLCommandQueue
    private var computePipelineState: MTLComputePipelineState
    public var ciContext: CIContext?
    
    required init(coder: NSCoder) {
     
        // Get the default metal device.
        let metalDevice = MTLCreateSystemDefaultDevice()!
        self.ciContext = CIContext.init(mtlDevice: metalDevice)
        // Create a command queue.
        self.commandQueue = metalDevice.makeCommandQueue()!
     
        // Create the metal library containing the shaders
        let bundle = Bundle.main
        let url = bundle.url(forResource: "default", withExtension: "metallib")
        let library = try! metalDevice.makeLibrary(filepath: url!.path)
     
        // Create a function with a specific name.
        let function = library.makeFunction(name: "colorKernel")!
     
        // Create a compute pipeline with the above function.
        self.computePipelineState = try! metalDevice.makeComputePipelineState(function: function)
     
        // Initialize the cache to convert the pixel buffer into a Metal texture.
        var textCache: CVMetalTextureCache?
        if CVMetalTextureCacheCreate(kCFAllocatorDefault, nil, metalDevice, nil, &textCache) != kCVReturnSuccess {
            fatalError("Unable to allocate texture cache.")
        }
        else {
            self.textureCache = textCache
        }
     
        // Initialize super.
        super.init(coder: coder)
     
        // Assign the metal device to this view.
        self.device = metalDevice
     
        // Enable the current drawable texture read/write.
        self.framebufferOnly = false
     
        // Disable drawable auto-resize.
        self.autoResizeDrawable = false
     
        // Set the content mode to aspect fit.
        self.contentMode = .scaleToFill
     
        // Change drawing mode based on setNeedsDisplay().
        self.enableSetNeedsDisplay = true
        self.isPaused = true
     
        // Set the content scale factor to the screen scale.
        self.contentScaleFactor = UIScreen.main.scale
     
        // Set the size of the drawable.
        self.drawableSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
    override func draw(_ rect: CGRect) {
        autoreleasepool {
            if rect.width > 0 && rect.height > 0 {
                self.render(self)
            }
        }
    }
    
    private func render(_ view: MTKView) {
     
        let screenScale = UIScreen.main.nativeScale
        self.drawableSize = CGSize(width: self.bounds.width*screenScale, height: self.bounds.height*screenScale)
        guard let pixelBuffer = self.pixelBuffer else { return }
        
        guard let currentDrawable = view.currentDrawable else {
            return
        }
        // Create a command buffer
        let commandBuffer = commandQueue.makeCommandBuffer()
        
        var scale:CGFloat = 1.0
        let ciImage = CIImage.init(cvImageBuffer: pixelBuffer)
        let vWidth = ciImage.extent.width
        let vHeight = ciImage.extent.height
        let fWidth = self.bounds.width*screenScale
        let fHeight = self.bounds.height*screenScale

        let scaleX = fWidth/vWidth
        let scaleY = fHeight/vHeight
        if(scaleX>=scaleY) {
            scale = scaleY
        } else {
            scale = scaleX
        }
        let xDiff = (fWidth-vWidth*scale)/2
        let yDiff = (fHeight-vHeight*scale)/2
                
        let scaleFilter = CIFilter(name: "CILanczosScaleTransform")!
        scaleFilter.setValue(ciImage, forKey: kCIInputImageKey)
        scaleFilter.setValue(NSNumber(value:Double(scale)), forKey: kCIInputScaleKey)
        scaleFilter.setValue(1.0, forKey:kCIInputAspectRatioKey)
        var outputImage = scaleFilter.value(forKey: kCIOutputImageKey) as! UIKit.CIImage
        
        if(self.firstFilterEnabled) {
            let colorPosterizeFilter = CIFilter.init(name: "CIColorPosterize")
            colorPosterizeFilter?.setValue(outputImage, forKey: "inputImage")
            if let destinationImage = colorPosterizeFilter?.outputImage {
                outputImage = destinationImage
            }
        }
        if(self.secondFilterEnabled) {
            let colorInvertFilter = CIFilter.init(name: "CIColorInvert")
            colorInvertFilter?.setValue(outputImage, forKey: "inputImage")
            if let destinationImage = colorInvertFilter?.outputImage {
                outputImage = destinationImage
            }
        }
        if(self.thirdFilterEnabled) {
            let blurFilter = CIFilter.init(name: "CIMotionBlur")
            blurFilter?.setValue(outputImage, forKey: "inputImage")
            if let destinationImage = blurFilter?.outputImage {
                outputImage = destinationImage
            }
        }
        
        //render into the metal texture
        self.ciContext?.render(outputImage,
                              to: currentDrawable.texture,
                              commandBuffer: commandBuffer,
                          bounds: CGRect(origin: CGPoint(x: -xDiff, y: -yDiff), size: view.drawableSize),
                      colorSpace: CGColorSpaceCreateDeviceRGB())
        
        //register where to draw the instructions in the command buffer once it executes
        commandBuffer?.present(currentDrawable)
        //commit the command to the queue so it executes
        commandBuffer?.commit()
        // Check if the pixel buffer exists
        
        /*if(self.pixelBuffer==nil) {
            let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
            let status = CVPixelBufferCreate(kCFAllocatorDefault, 600, 400, kCVPixelFormatType_32BGRA, attrs, &self.pixelBuffer)
        }*/
        
        /*guard let videoPixelBuffer = self.videoPixelBuffer else { return }
        self.pixelBuffer = self.resizeFrame(sourcePixelFrame: videoPixelBuffer, targetSize: MTLSize(width: 640, height: 480, depth: 32), resizeMode: .scaleAspectFill)

        guard let pixelBuffer = self.pixelBuffer else { return }

        let theImage = CIImage.init(cvImageBuffer: pixelBuffer)*/
        //let blurFilter = CIFilter.init(name: "CIMotionBlur")
        //blurFilter?.setValue(theImage, forKey: "inputImage")
        /*let scale = 3.0
        let scaleFilter = CIFilter(name: "CILanczosScaleTransform")!
        scaleFilter.setValue(theImage, forKey: kCIInputImageKey)
        scaleFilter.setValue(NSNumber(value:scale), forKey: kCIInputScaleKey)
        scaleFilter.setValue(1.0, forKey:kCIInputAspectRatioKey)
        let outputImage = scaleFilter.value(forKey: kCIOutputImageKey) as! UIKit.CIImage
        //var oBuffer:CVPixelBuffer?*/
        
        /*let cfnumPointer = UnsafeMutablePointer<UnsafeRawPointer>.allocate(capacity: 1)
        let cfnum = CFNumberCreate(kCFAllocatorDefault, .intType, cfnumPointer)
        let keys: [CFString] = [kCVPixelBufferCGImageCompatibilityKey, kCVPixelBufferCGBitmapContextCompatibilityKey, kCVPixelBufferBytesPerRowAlignmentKey]
        let values: [CFTypeRef] = [kCFBooleanTrue, kCFBooleanTrue, cfnum!]
        let keysPointer = UnsafeMutablePointer<UnsafeRawPointer?>.allocate(capacity: 1)
        let valuesPointer =  UnsafeMutablePointer<UnsafeRawPointer?>.allocate(capacity: 1)
        keysPointer.initialize(to: keys)
        valuesPointer.initialize(to: values)
        
        let options = CFDictionaryCreate(kCFAllocatorDefault, keysPointer, valuesPointer, keys.count, nil, nil)
        CVPixelBufferCreate(kCFAllocatorDefault, Int(outputImage.extent.width), Int(outputImage.extent.height), kCVPixelFormatType_32BGRA, options, &self.pixelBuffer)
        */
        //if let destinationImage = scaleFilter.outputImage {
        //    self.ciContext?.render(destinationImage, to: pixelBuffer)
        //}
        
        // Get width and height for the pixel buffer
        /*let width = CVPixelBufferGetWidth(pixelBuffer)
        let height = CVPixelBufferGetHeight(pixelBuffer)
     
        // Converts the pixel buffer in a Metal texture.
        var cvTextureOut: CVMetalTexture?
        CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault, self.textureCache!, pixelBuffer, nil, .bgra8Unorm, width, height, 0, &cvTextureOut)
        guard let cvTexture = cvTextureOut, let inputTexture = CVMetalTextureGetTexture(cvTexture) else {
            print("Failed to create metal texture")
            return
        }
     
        // Check if Core Animation provided a drawable.
        guard let drawable: CAMetalDrawable = self.currentDrawable else { return }
     
        // Create a command buffer
        let commandBuffer = commandQueue.makeCommandBuffer()
     
        // Create a compute command encoder.
        let computeCommandEncoder = commandBuffer?.makeComputeCommandEncoder()
     
        // Set the compute pipeline state for the command encoder.
        computeCommandEncoder?.setComputePipelineState(computePipelineState)
     
        // Set the input and output textures for the compute shader.
        computeCommandEncoder?.setTexture(inputTexture, index: 0)
        computeCommandEncoder?.setTexture(drawable.texture, index: 1)
     
        // Convert the time in a metal buffer.
        var time = Float(self.inputTime!)
        computeCommandEncoder?.setBytes(&time, length: MemoryLayout<Float>.size, index: 0)
     
        // Encode a threadgroup's execution of a compute function
        computeCommandEncoder?.dispatchThreadgroups(inputTexture.threadGroups(), threadsPerThreadgroup: inputTexture.threadGroupCount())
     
        // End the encoding of the command.
        computeCommandEncoder?.endEncoding()
     
        // Register the current drawable for rendering.
        commandBuffer?.present(drawable)
     
        // Commit the command buffer for execution.
        commandBuffer?.commit()*/
    }
     
    @objc public func resizeFrame(sourcePixelFrame:CVPixelBuffer, targetSize:MTLSize, resizeMode: PixelResizeMode) -> CVPixelBuffer? {
        
        guard let sourceTexture = makeTextureFromCVPixelBuffer(sourcePixelFrame) else {
            print("FrameMixer resize convert to texture failed")
            return nil
        }
        
        let queue = self.commandQueue
        let commandBuffer = queue.makeCommandBuffer()
        
        let device = queue.device;
        
        let descriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: sourceTexture.pixelFormat, width: targetSize.width, height: targetSize.height, mipmapped: false)
        descriptor.usage = [.shaderWrite, .shaderRead, .renderTarget]
        
        guard let desTexture = device.makeTexture(descriptor: descriptor) else {
                  print("FrameMixer resize makeTexture failed")
                  return nil
        }
        
        // Scale texture
        let sourceWidth = CVPixelBufferGetWidth(sourcePixelFrame)
        let sourceHeight = CVPixelBufferGetHeight(sourcePixelFrame)
        let widthRatio: Double = Double(targetSize.width) / Double(sourceWidth)
        let heightRatio: Double = Double(targetSize.height) / Double(sourceHeight)
        var scaleX: Double = 0;
        var scaleY: Double  = 0;
        var translateX: Double = 0;
        var translateY: Double = 0;
        
        if resizeMode == .scaleToFill {
            scaleX = Double(targetSize.width) / Double(sourceWidth)
            scaleY = Double(targetSize.height) / Double(sourceHeight)
            
        } else if resizeMode == .scaleAspectFit {
            if heightRatio > widthRatio {
                scaleX = Double(targetSize.width) / Double(sourceWidth)
                scaleY = scaleX
                let currentHeight = Double(sourceHeight) * scaleY
                translateY = (Double(targetSize.height) - currentHeight) * 0.5
            } else {
                scaleY = Double(targetSize.height) / Double(sourceHeight)
                scaleX = scaleY
                let currentWidth = Double(sourceWidth) * scaleX
                translateX = (Double(targetSize.width) - currentWidth) * 0.5
            }
            
        } else if resizeMode == .scaleAspectFill {
            if heightRatio > widthRatio {
                scaleY = Double(targetSize.height) / Double(sourceHeight)
                scaleX = scaleY
                let currentWidth = Double(sourceWidth) * scaleX
                translateX = (Double(targetSize.width) - currentWidth) * 0.5
                
            } else {
                scaleX = Double(targetSize.width) / Double(sourceWidth)
                scaleY = scaleX
                let currentHeight = Double(sourceHeight) * scaleY
                translateY = (Double(targetSize.height) - currentHeight) * 0.5
            }
        }
        
        var transform = MPSScaleTransform(scaleX: scaleX, scaleY: scaleY, translateX: translateX, translateY: translateY)
        let scale = MPSImageBilinearScale.init(device: device)
        withUnsafePointer(to: &transform) { (transformPtr: UnsafePointer<MPSScaleTransform>) -> () in
            scale.scaleTransform = transformPtr
            scale.encode(commandBuffer: commandBuffer!, sourceTexture: sourceTexture, destinationTexture: desTexture)
        }
        
        // Copy texture to buffer
        let bytesPerRow = CVPixelBufferGetBytesPerRow(sourcePixelFrame)
        guard let encoder = commandBuffer!.makeBlitCommandEncoder(),
            let textureBuffer = device.makeBuffer(length: bytesPerRow * descriptor.height, options: .storageModeShared)else {
                return nil
        }
        encoder.copy(from: desTexture,
                     sourceSlice: 0,
                     sourceLevel: 0,
                     sourceOrigin: MTLOrigin.init(x: 0, y: 0, z: 0),
                     sourceSize: MTLSize.init(width: desTexture.width, height: desTexture.height, depth: 1),
                     to: textureBuffer,
                     destinationOffset: 0,
                     destinationBytesPerRow: CVPixelBufferGetBytesPerRow(sourcePixelFrame),
                     destinationBytesPerImage: textureBuffer.length)
        encoder.endEncoding()
        commandBuffer!.commit()
        
        // Create CVPixelBuffer from buffer
        var resultBuffer: CVPixelBuffer?
        let pixelFormatType = CVPixelBufferGetPixelFormatType(sourcePixelFrame)
        CVPixelBufferCreateWithBytes(kCFAllocatorDefault,
                                     desTexture.width,
                                     desTexture.height,
                                     pixelFormatType,
                                     textureBuffer.contents(),
                                     CVPixelBufferGetBytesPerRow(sourcePixelFrame),
                                     nil,
                                     nil,
                                     nil,
                                     &resultBuffer)
        return resultBuffer
    }
    
    private func makeTextureFromCVPixelBuffer(_ pixelBuffer:CVPixelBuffer) -> MTLTexture? {
         
         guard let textureCache = textureCache else {
             print("FrameMixer make buffer failed, texture cache is not exist")
             return nil
         }
         
         let width = CVPixelBufferGetWidth(pixelBuffer)
         let height = CVPixelBufferGetHeight(pixelBuffer)
         
         var cvTextureOut:CVMetalTexture?
         
         let result = CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault, textureCache, pixelBuffer, nil, .bgra8Unorm, width, height, 0, &cvTextureOut)
         assert(result == kCVReturnSuccess)
         
         guard let cvTexture = cvTextureOut, let texture = CVMetalTextureGetTexture(cvTexture) else {
             print("FrameMixer make buffer failed to create preview texture")
             
             CVMetalTextureCacheFlush(textureCache, 0)
             return nil
         }
         
         return texture
         
     }

}

extension MTLTexture {
 
    func threadGroupCount() -> MTLSize {
        return MTLSizeMake(8, 8, 1)
    }
 
    func threadGroups() -> MTLSize {
        let groupCount = threadGroupCount()
        return MTLSizeMake(Int(self.width) / groupCount.width, Int(self.height) / groupCount.height, 1)
    }
}
