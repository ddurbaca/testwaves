//
//  ViewController.swift
//  test
//
//  Created by Dan Durbaca on 11/10/2020.
//  Copyright © 2020 Dan Durbaca. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class ViewController: UIViewController {
    
    @IBOutlet var metalView: MetalView!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var firstFilterButton: UIButton!
    @IBOutlet var secondFilterButton: UIButton!
    @IBOutlet var thirdFilterButton: UIButton!

    @IBAction func saveButtonPressed(sender: UIButton) {
        self.mixVideo()
    }
    @IBAction func firstFilterToggle(sender: UIButton) {
        firstFilterEnabled = !firstFilterEnabled
        self.firstFilterButton.isSelected = firstFilterEnabled
        self.metalView.firstFilterEnabled = firstFilterEnabled
    }
    @IBAction func secondFilterToggle(sender: UIButton) {
        secondFilterEnabled = !secondFilterEnabled
        self.secondFilterButton.isSelected = secondFilterEnabled
        self.metalView.secondFilterEnabled = secondFilterEnabled
    }
    @IBAction func thirdFilterToggle(sender: UIButton) {
        thirdFilterEnabled = !thirdFilterEnabled
        self.thirdFilterButton.isSelected = thirdFilterEnabled
        self.metalView.thirdFilterEnabled = thirdFilterEnabled
    }

    var firstFilterEnabled:Bool = false
    var secondFilterEnabled:Bool = false
    var thirdFilterEnabled:Bool = false

    var player: AVPlayer = AVPlayer()
    var currentVideo:Int = 0
    var shots = [AVPlayerItem]()
    var assets = [AVURLAsset]()
    var pvos = [AVPlayerItemVideoOutput]()
    var pvo:AVPlayerItemVideoOutput = {
        let attributes = [kCVPixelBufferPixelFormatTypeKey as String : Int(kCVPixelFormatType_32BGRA)]
        return AVPlayerItemVideoOutput(pixelBufferAttributes: attributes)
    }()
     
    lazy var displayLink: CADisplayLink = {
        let dl = CADisplayLink(target: self, selector: #selector(readBuffer(_:)))
        dl.add(to: .current, forMode: .default)
        dl.isPaused = true
        return dl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Get the video file url.
        guard let url1 = Bundle.main.url(forResource: "video1", withExtension: "mp4") else {
            print("Impossible to find the video.")
            return
        }
         guard let url2 = Bundle.main.url(forResource: "video2", withExtension: "mp4") else {
             print("Impossible to find the video.")
             return
         }
        guard let url3 = Bundle.main.url(forResource: "video3", withExtension: "mp4") else {
            print("Impossible to find the video.")
            return
        }

        var firstAsset: AVURLAsset!, secondAsset: AVURLAsset!, thirdAsset: AVURLAsset!
        firstAsset = AVURLAsset(url: url1)
        secondAsset = AVURLAsset(url: url2)
        thirdAsset  = AVURLAsset(url: url3)

        self.assets = [firstAsset, secondAsset, thirdAsset]

        for item in assets {
            let shot = AVPlayerItem(asset: item)
            shots.append(shot)
        }

        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.donePlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        self.pvo = {
            let attributes = [kCVPixelBufferPixelFormatTypeKey as String : Int(kCVPixelFormatType_32BGRA)]
            return AVPlayerItemVideoOutput(pixelBufferAttributes: attributes)
        }()
        shots[self.currentVideo].add(self.pvo)
        player.replaceCurrentItem(with: shots[self.currentVideo])
    }
    
    @objc func donePlaying(note: NSNotification) {
        print("Video Finished")
        if(self.currentVideo<2) {
            self.currentVideo = self.currentVideo+1
            self.pvo = {
                let attributes = [kCVPixelBufferPixelFormatTypeKey as String : Int(kCVPixelFormatType_32BGRA)]
                return AVPlayerItemVideoOutput(pixelBufferAttributes: attributes)
            }()
            shots[self.currentVideo].add(self.pvo)
            player.replaceCurrentItem(with: shots[self.currentVideo])
            player.play()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     
        // Resume the display link
        displayLink.isPaused = false
     
        // Start to play
        player.play()
    }
    
    @objc private func readBuffer(_ sender: CADisplayLink) {
        
        var currentTime = CMTime.invalid
        let nextVSync = sender.timestamp + sender.duration
        let playerItemVideoOutput = self.pvo//s[self.currentVideo]
        currentTime = playerItemVideoOutput.itemTime(forHostTime: nextVSync)
        
        if playerItemVideoOutput.hasNewPixelBuffer(forItemTime: currentTime), let pixelBuffer = playerItemVideoOutput.copyPixelBuffer(forItemTime: currentTime, itemTimeForDisplay: nil) {
               
                self.metalView.pixelBuffer = pixelBuffer
               
                self.metalView.inputTime = currentTime.seconds
        }
    }
    
    func mixVideo() {
        /*let vidComp = AVVideoComposition(asset: avAsset,
           applyingCIFiltersWithHandler: {
           request in
               filtered = request.sourceImage.imageByClampingToExtent();
               filtered = filtered.imageByApplyingFilter("CIGaussianBlur",
                   withInputParameters: [kCIInputRadiusKey: 100])
               filtered = filtered.imageByCroppingToRect(request.sourceImage.extent)
               request.finishWithImage(filtered, context: cicontext)
        })
        let export = AVAssetExportSession(asset: avAsset,
            presetName: AVAssetExportPreset1920x1080)
        export.outputFileType = AVFileTypeQuickTimeMovie
        export.outputURL = outURL
        export.videoComposition = vidComp
        FileManager.defaultManager().removeItemAtURL(outURL)
        export.exportAsynchronouslyWithCompletionHandler()*/
        let composition = AVMutableComposition.init()

        let videoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        videoComposition.renderScale  = 1.0

        let compositionCommentaryTrack: AVMutableCompositionTrack? = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)


        let compositionVideoTrack: AVMutableCompositionTrack? = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)


        let clipVideoTrack1:AVAssetTrack = self.assets[0].tracks(withMediaType: AVMediaType.video)[0]
        let clipVideoTrack2:AVAssetTrack = self.assets[1].tracks(withMediaType: AVMediaType.video)[0]
        let clipVideoTrack3:AVAssetTrack = self.assets[2].tracks(withMediaType: AVMediaType.video)[0]

        let audioTrack1: AVAssetTrack? = self.assets[0].tracks(withMediaType: AVMediaType.audio)[0]
        let audioTrack2: AVAssetTrack? = self.assets[1].tracks(withMediaType: AVMediaType.audio)[0]
        let audioTrack3: AVAssetTrack? = self.assets[2].tracks(withMediaType: AVMediaType.audio)[0]

        var insert_at = CMTime.zero
        
        try? compositionCommentaryTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: self.assets[0].duration), of: audioTrack1!, at: CMTime.zero)

        try? compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: self.assets[0].duration), of: clipVideoTrack1, at: CMTime.zero)
        insert_at = insert_at + self.assets[0].duration
        
        try? compositionCommentaryTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: self.assets[1].duration), of: audioTrack2!, at: insert_at)

        try? compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: self.assets[1].duration), of: clipVideoTrack2, at: insert_at)
        insert_at = insert_at + self.assets[1].duration

        try? compositionCommentaryTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: self.assets[2].duration), of: audioTrack3!, at: insert_at)

        try? compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: self.assets[2].duration), of: clipVideoTrack3, at: insert_at)

        let isPortrait = !UIDevice.current.orientation.isLandscape
        var naturalSize = clipVideoTrack1.naturalSize
        
        var scale2:CGFloat = 1.0
        let vWidth2 = clipVideoTrack2.naturalSize.width
        let vHeight2 = clipVideoTrack2.naturalSize.height
        
        let scaleX2 = naturalSize.width/vWidth2
        let scaleY2 = naturalSize.height/vHeight2
        if(scaleX2>=scaleY2) {
            scale2 = scaleY2
        } else {
            scale2 = scaleX2
        }
        let xDiff2 = (naturalSize.width-vWidth2*scale2)/2
        let yDiff2 = (naturalSize.height-vHeight2*scale2)/2
        
        var scale3:CGFloat = 1.0
        let vWidth3 = clipVideoTrack3.naturalSize.width
        let vHeight3 = clipVideoTrack3.naturalSize.height
        
        let scaleX3 = naturalSize.width/vWidth3
        let scaleY3 = naturalSize.height/vHeight3
        if(scaleX3>=scaleY3) {
            scale3 = scaleY3
        } else {
            scale3 = scaleX3
        }
        let xDiff3 = (naturalSize.width-vWidth3*scale3)/2
        let yDiff3 = (naturalSize.height-vHeight3*scale3)/2

        if isPortrait
        {
            //naturalSize = CGSize.init(width: naturalSize.height, height: naturalSize.width)
        }

        videoComposition.renderSize = naturalSize

        let scale = CGFloat(1.0)

        var transform = CGAffineTransform.init(scaleX: CGFloat(scale), y: CGFloat(scale))

        if isPortrait {
            //transform = transform.translatedBy(x: naturalSize.width, y: 0)
            //transform = transform.rotated(by: CGFloat(M_PI_2))
        } else {
            //transform = transform.translatedBy(x: naturalSize.width, y: naturalSize.height)
            //transform = transform.rotated(by: .pi)
        }

        let frontLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: compositionVideoTrack!)
        frontLayerInstruction.setTransform(transform, at: CMTime.zero)

        var transform2 = CGAffineTransform.init(scaleX: CGFloat(scale2), y: CGFloat(scale2))
        transform2 = transform2.translatedBy(x: xDiff2, y: yDiff2)
        frontLayerInstruction.setTransform(transform2, at: CMTime.zero+self.assets[0].duration)

        var transform3 = CGAffineTransform.init(scaleX: CGFloat(scale3), y: CGFloat(scale3))
        transform3 = transform3.translatedBy(x: xDiff3, y: yDiff3)
        frontLayerInstruction.setTransform(transform3, at: CMTime.zero+self.assets[0].duration+self.assets[1].duration)

        let MainInstruction = AVMutableVideoCompositionInstruction()
        MainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: composition.duration)
        MainInstruction.layerInstructions = [frontLayerInstruction]
        
        videoComposition.instructions = [MainInstruction]

        let parentLayer = CALayer.init()
        parentLayer.frame = CGRect.init(x: 0, y: 0, width: naturalSize.width, height: naturalSize.height)

        let videoLayer = CALayer.init()
        videoLayer.frame = parentLayer.frame

        let widthScale = self.metalView.frame.size.width/naturalSize.width

        parentLayer.addSublayer(videoLayer)

        videoComposition.animationTool = AVVideoCompositionCoreAnimationTool.init(postProcessingAsVideoLayer: videoLayer, in: parentLayer)

        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let videoPath = documentsPath+"/cropEditVideo.mov"

        let fileManager = FileManager.default

        if fileManager.fileExists(atPath: videoPath)
        {
            try! fileManager.removeItem(atPath: videoPath)
        }

        print("video path \(videoPath)")
        let finalVideoComposition = AVMutableVideoComposition(asset: composition, applyingCIFiltersWithHandler: {
           request in
            var filtered = request.sourceImage.clampedToExtent();
            if(self.firstFilterEnabled) {
                filtered = filtered.applyingFilter("CIColorPosterize",
                                                   parameters: [:])
            }
            if(self.secondFilterEnabled) {
                filtered = filtered.applyingFilter("CIColorInvert",
                                                   parameters: [:])
            }
            if(self.firstFilterEnabled) {
                filtered = filtered.applyingFilter("CIMotionBlur",
                                                   parameters: [:])
            }
            request.finish(with: filtered, context: self.metalView.ciContext)
        })
        var exportSession = AVAssetExportSession.init(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        exportSession?.videoComposition = finalVideoComposition
        exportSession?.outputFileType = AVFileType.mov
        exportSession?.outputURL = URL.init(fileURLWithPath: videoPath)
        exportSession?.videoComposition = finalVideoComposition
        var exportProgress: Float = 0
        let queue = DispatchQueue(label: "Export Progress Queue")
        queue.async(execute: {() -> Void in
            while exportSession != nil {
                //                int prevProgress = exportProgress;
                exportProgress = (exportSession?.progress)!
                print("current progress == \(exportProgress)")
                sleep(1)
            }
        })

        exportSession?.exportAsynchronously(completionHandler:{
            
            
            guard
                exportSession!.status == AVAssetExportSession.Status.completed,
                let outputURL = exportSession!.outputURL
              else { return }

            let saveVideoToPhotos = {
              let changes: () -> Void = {
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: outputURL)
              }
              PHPhotoLibrary.shared().performChanges(changes) { saved, error in
                DispatchQueue.main.async {
                  let success = saved && (error == nil)
                  let title = success ? "Success" : "Error"
                  let message = success ? "Video saved" : "Failed to save video"

                  let alert = UIAlertController(
                    title: title,
                    message: message,
                    preferredStyle: .alert)
                  alert.addAction(UIAlertAction(
                    title: "OK",
                    style: UIAlertAction.Style.cancel,
                    handler: nil))
                  self.present(alert, animated: true, completion: nil)
                }
              }
            }
            // 5
            if PHPhotoLibrary.authorizationStatus() != .authorized {
                PHPhotoLibrary.requestAuthorization { status in
                    if status == .authorized {
                        saveVideoToPhotos()
                    }
                }
            } else {
                    saveVideoToPhotos()
            }

        })
/*        exportSession?.exportAsynchronously(completionHandler: {


            if exportSession?.status == AVAssetExportSession.Status.failed
            {
                print("Failed \(exportSession?.error)")
            } else if exportSession?.status == AVAssetExportSession.Status.completed
            {
                exportSession = nil

                let asset = AVAsset.init(url: URL.init(fileURLWithPath: videoPath))
                DispatchQueue.main.async {
                    let item = AVPlayerItem.init(asset: asset)


                    self.player.replaceCurrentItem(with: item)

                    let assetDuration = CMTimeGetSeconds(composition.duration)
                    
                }

            }
        })*/
    }
    
    func exportDidFinish(_ session: AVAssetExportSession)-> Void {

    guard
      session.status == AVAssetExportSession.Status.completed,
      let outputURL = session.outputURL
      else { return }

    let saveVideoToPhotos = {
      let changes: () -> Void = {
        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: outputURL)
      }
      PHPhotoLibrary.shared().performChanges(changes) { saved, error in
        DispatchQueue.main.async {
          let success = saved && (error == nil)
          let title = success ? "Success" : "Error"
          let message = success ? "Video saved" : "Failed to save video"

          let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert)
          alert.addAction(UIAlertAction(
            title: "OK",
            style: UIAlertAction.Style.cancel,
            handler: nil))
          self.present(alert, animated: true, completion: nil)
        }
      }
    }
    // 5
    if PHPhotoLibrary.authorizationStatus() != .authorized {
        PHPhotoLibrary.requestAuthorization { status in
            if status == .authorized {
                saveVideoToPhotos()
            }
        }
    } else {
            saveVideoToPhotos()
        }
    }
}

